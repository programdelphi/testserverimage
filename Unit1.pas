unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, IPPeerClient, FMX.StdCtrls, FMX.ListBox, FMX.ScrollBox,
  FMX.Memo, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, FMX.Objects, FMX.Controls.Presentation, FMX.Edit,
  FMX.EditBox, FMX.SpinBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.StorageJSON;

type
  TForm1 = class(TForm)
    edtServerAddress: TEdit;
    Image1: TImage;
    RESTClient1: TRESTClient;
    RESTResponse1: TRESTResponse;
    RESTRequest1: TRESTRequest;
    memResult: TMemo;
    cbbCode: TComboBox;
    Label1: TLabel;
    btnExecute: TButton;
    lbl1: TLabel;
    lbl2: TLabel;
    Button1: TButton;
    lblCount: TLabel;
    sbTimebettwennsales1: TSpinBox;
    edtBarCode: TEdit;
    chk1: TCheckBox;
    lblDescription: TLabel;
    lblStatus: TLabel;
    lblError: TLabel;
    FDMemTable1: TFDMemTable;
    FDMemTable1Desc: TStringField;
    FDMemTable1Status: TStringField;
    FDMemTable1Error: TStringField;
    chkRecord: TCheckBox;
    lblRecords: TLabel;
    btnClearTable: TButton;
    btnSaveTable: TButton;
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    lblErrorCount: TLabel;
    Timer1: TTimer;
    procedure btnExecuteClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnClearTableClick(Sender: TObject);
    procedure btnSaveTableClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    procedure DispalyInfo;
    procedure ExecuteError(Sender: TObject);
    procedure ContinueCheck;
    { Private declarations }
  public
    aCumTime: Double;
    aReadyForNext: Boolean;
    aCount: Integer;
    aupc: Integer;
    aErrorCount: Integer;
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  System.JSON, Data.DBXJSONCommon, System.Diagnostics;

{$R *.fmx}

procedure TForm1.btnClearTableClick(Sender: TObject);
begin
  FDMemTable1.Close;
  FDMemTable1.Open;
  lblRecords.Text := Format('Record Count: %d', [FDMemTable1.RecordCount]);
end;

procedure TForm1.btnExecuteClick(Sender: TObject);
var
  aBarcode: string;
begin
  Beep;
  aBarcode := edtBarCode.Text.Trim;
  if aBarcode.IsEmpty then
    aBarcode := cbbCode.Items[cbbCode.ItemIndex];
  RESTClient1.BaseURL := Format(edtServerAddress.Text, [aBarcode]);
  try
    RESTRequest1.ExecuteAsync(DispalyInfo, True, True, ExecuteError);
    Inc(aCount);
  except
  end;
end;

procedure TForm1.btnSaveTableClick(Sender: TObject);
begin

  FDMemTable1.SaveToFile('C:\Temp\M'+FormatDateTime('yyyymmddhhmmss', Now)+'.txt', sfJSON);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  x, z: Integer;
begin
  x := 5;
  aReadyForNext := True;

  aReadyForNext := False;
  RESTClient1.BaseURL := Format(edtServerAddress.Text, [aupc.ToString]);
  Beep;
  try
    RESTRequest1.ExecuteAsync(DispalyInfo, True, True, ExecuteError);
    x := x + 1;
    Inc(aCount);
  except
  end;
  if chk1.IsChecked and (not Timer1.Enabled) then
    Timer1.Enabled := True;
end;

procedure TForm1.DispalyInfo;
var
  AJson: TJSONObject;
  AJsonA: TJSONArray;
  AStream: TStream;
  ADesc, AStatus, AError: string;
begin
  AJson := RESTResponse1.JSONValue as TJSONObject;
  AJson.TryGetValue<string>('Desc', ADesc);
  AJson.TryGetValue<string>('Status', AStatus);
  AJson.TryGetValue<string>('Error', AError);
  AJson.TryGetValue<TJSONArray>('Image', AJsonA);

  lblDescription.Text := ADesc;
  lblStatus.Text := AStatus;
  lblError.Text := AError;
  if AJsonA.Count > 0 then
  begin
    AStream := TDBXJSONTools.JSONToStream(AJsonA);
    try
      AStream.Position := 0;
      Image1.Bitmap.LoadFromStream(AStream);
    finally
      AStream.Free;
    end;
  end;
  if chkRecord.IsChecked then
  begin
    FDMemTable1.Insert;
    FDMemTable1Desc.AsString := ADesc;
    FDMemTable1Status.AsString := AStatus;
    FDMemTable1Error.AsString := AError;
    FDMemTable1.Post;
    lblRecords.Text := Format('Record Count: %d', [FDMemTable1.RecordCount]);
  end;
  {if chk1.IsChecked then
    ContinueCheck;}
end;

procedure TForm1.ExecuteError(Sender: TObject);
begin
    Inc(aErrorCount);
    lblErrorCount.Text := Format('Error Count: %d', [aErrorCount]);
end;

procedure TForm1.ContinueCheck;
begin
  Inc(aupc);
  lblCount.Text := aCount.ToString;
  aReadyForNext := True;
  if aCount < 10000 then
    Button1Click(nil);
  if aupc = 20 then
    aupc := 1;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  aCount := 0;
  aErrorCount := 0;
  aupc := 1;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  ContinueCheck;
end;

end.
